﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScript : MonoBehaviour { 
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(gameManager.instance.cam.transform.position.x, gameManager.instance.cam.transform.position.y - 8, 0f);
	}
}
