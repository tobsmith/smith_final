﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakCubeScript : MonoBehaviour
{

    void OnTriggerStay2D(Collider2D other)
    {
        //print("Dummy collision");
        if (other.tag == "Enemy")
        {
            Destroy(transform.parent.gameObject);
        }
    }
}
