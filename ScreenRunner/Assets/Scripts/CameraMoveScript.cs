﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoveScript : MonoBehaviour {

	[HideInInspector]public float cameraMoving;
    private float updateDelay = 0.4f;
	// Use this for initialization
	void Start () {
		cameraMoving = gameManager.instance.camSpeed;
	}
	
	// Update is called once per frame
	void Update () {
        if (gameManager.instance.gameStart)
        {
            cameraMoving = gameManager.instance.camSpeed;
            Invoke("moveCameraHorizontally", updateDelay);
        }
	}

    void moveCameraHorizontally()
    {
        
        transform.position += new Vector3(cameraMoving, 0, 0);
        if(!gameManager.instance.isPlayerDead)
            gameManager.instance.points += 1;
    }
}
