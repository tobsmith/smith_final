﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderPositionScript : MonoBehaviour {
    // Update is called once per frame
    private Vector3 newPosition;
	void Update () {
        newPosition = new Vector3(gameManager.instance.playerPos.x, gameManager.instance.playerPos.y, 0);
        transform.position = newPosition;
	}
}
