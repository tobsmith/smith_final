﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemyScript : MonoBehaviour {

    private bool onScreen = false;
    public float enemyMovementSpeed;
    public int pointsPerKill = 100;
	
	// Update is called once per frame
	void Update () {
        if (transform.position.x < gameManager.instance.rightScreenBound - 1)
        {
            onScreen = true;
            //print("Enemy Transform: " + transform.position.x + "Right Screen Bound " + gameManager.instance.rightScreenBound);
        }

        if(onScreen && (transform.position.x > gameManager.instance.rightScreenBound || transform.position.x < gameManager.instance.leftScreenBound))
        {
            death();
            //print("enemy died at " + transform.position.x + " and the right screen bound was " + gameManager.instance.rightScreenBound);
        }

        if(onScreen && !(gameManager.instance.isPlayerDead) && gameManager.instance.gameStart)
        {
            if (transform.position.y > gameManager.instance.playerPos.y)
            {
                if (transform.position.x > gameManager.instance.playerPos.x)
                    transform.position += new Vector3(-1 * enemyMovementSpeed, -1 * enemyMovementSpeed, 0);
                else
                    transform.position += new Vector3(enemyMovementSpeed, -1 * enemyMovementSpeed, 0);
            }
            else if (transform.position.y < gameManager.instance.playerPos.y)
            {
                if (transform.position.x > gameManager.instance.playerPos.x)
                    transform.position += new Vector3(-1 * enemyMovementSpeed, enemyMovementSpeed, 0);
                else
                    transform.position += new Vector3(enemyMovementSpeed, enemyMovementSpeed, 0);
            }
            else
            {
                if (transform.position.x > gameManager.instance.playerPos.x)
                    transform.position += new Vector3(-1 * enemyMovementSpeed, 0, 0);
                else
                    transform.position += new Vector3(enemyMovementSpeed, 0, 0);
            }

        }
	}

    void death()
    {
        gameManager.instance.points += pointsPerKill;
        this.gameObject.SetActive(false);
    }

}
