﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundScript : MonoBehaviour {
	
	private float rightCornerX;

	void Start () {
		rightCornerX = transform.position.x + 5;
	}
	
	// Update is called once per frame
	void Update () {
		if (rightCornerX < gameManager.instance.leftScreenBound) {
			gameManager.instance.currPlatforms--;
			Destroy (gameObject);
		}
	}
}
