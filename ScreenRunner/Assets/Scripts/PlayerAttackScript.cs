﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackScript : MonoBehaviour {

    public Vector2 attackForce;

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            other.GetComponent<Rigidbody2D>().AddForce(attackForce);
        }
    }
}
