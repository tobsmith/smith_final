﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimplePlatformController : MonoBehaviour {

    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jump = false;
    private bool release = false;

    public float moveForce = 2400f; //og was 365
    public float maxSpeed = 10f; //og was 5
    public float jumpForce = 1000f;
    public Transform groundCheck;
    private bool hasHitSpace = false;

    private bool grounded = false;
    private Animator anim;
    private Rigidbody2D rb2d;

    //for colliders
    public GameObject right;
    public GameObject top;
    public GameObject left;
    public GameObject down;
    public float punchTime = 1;

    //health stuff
    public Slider health;
    public int playerHealth = 4;
    private int startingHealth;
    [HideInInspector] public float playerHealthPercent = 1f;

    //gameover text
    public Text GameOver;



	void Awake () {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

        right.SetActive(false);
        top.SetActive(false);
        left.SetActive(false);
        down.SetActive(false);
        GameOver.gameObject.SetActive(false);
        //gameObject.SetActive(false);
    }
    private void Start()
    {
        startingHealth = playerHealth;
        health.value = playerHealthPercent;
    }
    // Update is called once per frame
    void Update () { 

        if(gameManager.instance.gameStart)
        {
            rb2d.sleepMode = RigidbodySleepMode2D.NeverSleep;
        }

        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        
        gameManager.instance.playerPos = new Vector2(transform.position.x, transform.position.y);

        if(Input.GetButtonDown("Jump") && grounded)
        {
            jump = true;
            if(!hasHitSpace)
            {
                hasHitSpace = true;
            }
            
        }

        if (Input.GetButtonUp("Jump"))
        {
            release = true;
        }

		if (transform.position.x <= gameManager.instance.leftScreenBound || transform.position.x >= gameManager.instance.rightScreenBound ||
            transform.position.y > gameManager.instance.upperScreenBound || transform.position.y <= gameManager.instance.lowerScreenBound || playerHealth <= 0) {
            //print("player's x value " + transform.position.x);
            //print ("left side " + gameManager.instance.leftScreenBound + " right side " + gameManager.instance.rightScreenBound);
            playerHealthPercent = 0;
            health.value = playerHealthPercent;
			Death ();
		}

        if (Input.GetButtonDown("Z"))
        {
            right.SetActive(true);
            Invoke("DisableRight", punchTime);
        }
        if (Input.GetButtonDown("LeftShift"))
        {
            top.SetActive(true);
            Invoke("DisableTop", punchTime);
        }
        if (Input.GetButtonDown("Fire1"))
        {
            left.SetActive(true);
            Invoke("DisableLeft", punchTime);
        }
        if (Input.GetButtonDown("LeftAlt"))
        {
            down.SetActive(true);
            Invoke("DisableDown", punchTime);
        }

    }

    private void DisableRight()
    {
        right.SetActive(false);
    }

    private void DisableTop()
    {
        top.SetActive(false);
    }

    private void DisableLeft()
    {
        left.SetActive(false);
    }

    private void DisableDown()
    {
        down.SetActive(false);
    }

    void Death() {
		this.gameObject.SetActive(false);
		gameManager.instance.isPlayerDead = true;
        GameOver.gameObject.SetActive(true);
        Text goText = GameOver.gameObject.GetComponentInChildren<Text>();
        goText.text = "\nYour final point total is: " + gameManager.instance.points + "\nPress Enter to return to start screen";
        //print("Died at " + transform.position.y);
	}

    void FixedUpdate()
	{
		float h = Input.GetAxis ("Horizontal");

		anim.SetFloat ("Speed", Mathf.Abs (h));

		if (h * rb2d.velocity.x < maxSpeed)
			rb2d.AddForce (Vector2.right * h * moveForce);

		if (Mathf.Abs (rb2d.velocity.x) > maxSpeed)
			rb2d.velocity = new Vector2 (Mathf.Sign (rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

		if (h > 0 && !facingRight)
			Flip ();
		else if (h < 0 && facingRight)
			Flip ();

		if (jump && hasHitSpace) {
            anim.SetTrigger ("Jump");
			rb2d.AddForce (new Vector2 (0f, jumpForce));
			jump = false;
		} 
        if(release)
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, (rb2d.velocity.y * 0.5f));
            release = false;
        }
	}

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Enemy")
        {
            playerHealth--;
            playerHealthPercent = (float) playerHealth / startingHealth;
            health.value = playerHealthPercent;
            //print(playerHealthPercent);
            //print(playerHealth);
            Destroy(other.gameObject);
            //consider slowing player down and increasing the enemy knockback instead
        }
    }
}
