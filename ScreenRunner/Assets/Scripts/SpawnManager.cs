﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    //parameters for platforms
    public int maxPlatforms = 10;
    public GameObject platform;
    public float horizontalMin = 10;
    public float horizontalMax = 10;
    public float verticalMin = 0f;
    public float verticalMax = 0f;

    //parameters for enemies
    public GameObject flyingEnemy;
    public int odds = 5;
    public int maxHeight = 12;

    //obsticals
    public GameObject ob1;
    public GameObject ob2;
    public GameObject ob3;
    public GameObject ob4;
    //minimum platforms between objects
    public int minPlats = 4;
    //maximum platforms between objects
    public int maxPlats = 6;
    private int betweenObsticles;


    public Vector2 originPosition;

	// Use this for initialization
	void Start () {
        originPosition = transform.position;
        betweenObsticles = Random.Range(minPlats, maxPlats);
        Spawn();
	}

	void Update() {
		if (gameManager.instance.currPlatforms < maxPlatforms) {
            obsticleSpawn();
			gameManager.instance.currPlatforms++;
		}
	}

	void singleSpawn() {
		Vector2 newPositon = originPosition + new Vector2 (horizontalMax, verticalMax);
		Instantiate (platform, newPositon, Quaternion.identity);
		originPosition = newPositon;
        int spawnOdds = Random.Range(1, odds);
        if(spawnOdds == 1)
        {
            Instantiate(flyingEnemy, new Vector2(Random.Range(newPositon.x, newPositon.x + 10), Random.Range(verticalMax, maxHeight)), Quaternion.identity);
        }
	}
    
    

    void obsticleSpawn()
    {
        if (betweenObsticles == 0)
        {
            betweenObsticles = Random.Range(minPlats, maxPlats);
            obsticleManager();
        }
        else
            singleSpawn();
        betweenObsticles--;
        //print("Before Call: " + (betweenObsticles + 1) + " After Call: " + betweenObsticles);
    }

    void obsticleManager()
    {
        int obSelect = Random.Range(1, 5);
        switch(obSelect)
        {
            case 1:
                spawnOb1();
                break;
            case 2:
                spawnOb2();
                break;
            case 3:
                spawnOb3();
                break;
            case 4:
                spawnOb4();
                break;
        }
        print("Obsticle " + obSelect + " was spawned");
        singleSpawn();
    }
    
    void spawnOb1()
    {
        Instantiate(ob1, new Vector3(originPosition.x, (originPosition.y + 3.5F), 0f), Quaternion.identity);
    }

    void spawnOb2()
    {
        Instantiate(ob2, new Vector3(originPosition.x, (originPosition.y + 3F), 0f), Quaternion.identity);
    }

    void spawnOb3()
    {
        Instantiate(ob3, new Vector3(originPosition.x, (originPosition.y + 6), 0f), Quaternion.Euler(new Vector3(0, 0, 90)));
    }

    void spawnOb4()
    {
        Instantiate(ob4, new Vector3((originPosition.x + 15), (originPosition.y + 4), 0f), Quaternion.identity);
        originPosition += new Vector2(20, verticalMin);
    }

    void Spawn()
    {
        for (int i = 0; i < maxPlatforms; i++)
        {
			singleSpawn();
        }
    }
}
