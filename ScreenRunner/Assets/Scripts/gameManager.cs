﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameManager : MonoBehaviour {

    public static gameManager instance = null;
    [HideInInspector] public float leftScreenBound;
    [HideInInspector] public float rightScreenBound;
	public Camera cam;
	[HideInInspector] public float camSpeed = 0.1f;
	[HideInInspector] public bool isPlayerDead = false;
	[HideInInspector] public int currPlatforms = 10;
    [HideInInspector] public Vector2 playerPos;
    public float upperScreenBound = 16;
    public float lowerScreenBound = 0;
    [HideInInspector] public int points = 0;
    public Text pointText;
    [HideInInspector] public bool gameStart = false;
    public Image start;

    //player reference

    void Awake ()
    {
        if (instance == null)
            instance = this;
        else if (instance != null)
            Destroy(gameObject);
		cam = Camera.main;
		leftScreenBound = cam.transform.position.x - 17;
		rightScreenBound = cam.transform.position.x + 17;
        pointText = GameObject.FindWithTag("Points").GetComponent<Text>();
        start = GameObject.FindWithTag("Start").GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        leftScreenBound = cam.transform.position.x - 17;
        rightScreenBound = cam.transform.position.x + 17;
        //print("left screen bound " + leftScreenBound + " right screen bound " + rightScreenBound);

		if (isPlayerDead)
        {
            camSpeed = 0;

        }
        pointText.text = "Points: " + points;

        if (isPlayerDead && Input.GetButtonDown("Submit"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if(!gameStart && Input.GetButtonDown("Submit")) 
        {
            start.gameObject.SetActive(false);
            //player.gameObject.SetActive(true);
            gameStart = true;
        }
    }


}
